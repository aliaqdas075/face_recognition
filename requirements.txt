django
django-tastypie
gunicorn
whitenoise
djangorestframework
django-extensions
requests
cmake
dlib
face-recognition
numpy
opencv-python