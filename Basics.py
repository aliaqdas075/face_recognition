import cv2
import numpy
import face_recognition

imgElon = face_recognition.load_image_file('images_basic/Elon Musk.jpg')
imgElon = cv2.cvtColor(imgElon, cv2.COLOR_BGR2RGB)
imgTest = face_recognition.load_image_file('images_basic/Elon Musk 2.jpg')
imgTest = cv2.cvtColor(imgTest, cv2.COLOR_BGR2RGB)

faceLoc = face_recognition.face_locations(imgElon)[0]
encodeElon = face_recognition.face_encodings(imgElon)[0]
cv2.rectangle(imgElon, (faceLoc[3], faceLoc[0]), (faceLoc[1], faceLoc[2]), (255, 0, 255), 2)

faceLoc2 = face_recognition.face_locations(imgTest)[0]
encodeElon2 = face_recognition.face_encodings(imgTest)[0]
cv2.rectangle(imgTest, (faceLoc2[3], faceLoc2[0]), (faceLoc2[1], faceLoc2[2]), (255, 0, 255), 2)

results = face_recognition.compare_faces([encodeElon], encodeElon2)
faceDis = face_recognition.face_distance([encodeElon], encodeElon2)
print(results, faceDis)

cv2.putText(imgTest,f'{results} {round(faceDis[0],2)}',(50,50),cv2.FONT_HERSHEY_COMPLEX,1,(0,0,255),2)

cv2.imshow('Elon Musk', imgElon)
cv2.imshow('Elon Musk 2', imgTest)
cv2.waitKey(0)
